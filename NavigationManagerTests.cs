using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;


public class NavigationManagerTests
{
/*
    -10 0 10	-5 0 10	    0 0 10  	5 0 10	    10 0 10
    -10 0 5	    -5 0 5	    0 0 5	    5 0 5	    10 0 5
    -10 0 0	    -5 0 0	    0 0 0	    5 0 0	    10 0 0 
    -10 0 -5	-5 0 -5	    0 0 -5	    5 0 -5	    10 0 -5
    -10 0 -10	-5 0 -10	0 0 -10	    5 0 -10	    10 0 -10
*/
    [Test]
    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        -10.0f, 0.0f, 10.0f,    // position x,y,z,
        "Terrain_(-15.0, 0.0, 10.0)",     // expected left neighbor name
        "Terrain_(-10.0, 0.0, 15.0)",      // expected top neighbor name
        "Terrain_(-5.0, 0.0, 10.0)",      // expected right neighbor name
        "Terrain_(-10.0, 0.0, 5.0)")]    // expected bottom neighbor name
    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        -10.0f, 0.0f, 5.0f,    // position x,y,z,
        "Terrain_(-15.0, 0.0, 5.0)",     // expected left neighbor name
        "Terrain_(-10.0, 0.0, 10.0)",      // expected top neighbor name
        "Terrain_(-5.0, 0.0, 5.0)",      // expected right neighbor name
        "Terrain_(-10.0, 0.0, 0.0)")]    // expected bottom neighbor name
    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        -10.0f, 0.0f, 0.0f,    // position x,y,z,
        "Terrain_(-15.0, 0.0, 0.0)",     // expected left neighbor name
        "Terrain_(-10.0, 0.0, 5.0)",      // expected top neighbor name
        "Terrain_(-5.0, 0.0, 0.0)",      // expected right neighbor name
        "Terrain_(-10.0, 0.0, -5.0)")]    // expected bottom neighbor name
   [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        -10.0f, 0.0f, -5.0f,    // position x,y,z,
        "Terrain_(-15.0, 0.0, -5.0)",     // expected left neighbor name
        "Terrain_(-10.0, 0.0, 0.0)",      // expected top neighbor name
        "Terrain_(-5.0, 0.0, -5.0)",      // expected right neighbor name
        "Terrain_(-10.0, 0.0, -10.0)")]    // expected bottom neighbor name
    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        -10.0f, 0.0f, -10.0f,    // position x,y,z,
        "Terrain_(-15.0, 0.0, -10.0)",     // expected left neighbor name
        "Terrain_(-10.0, 0.0, -5.0)",      // expected top neighbor name
        "Terrain_(-5.0, 0.0, -10.0)",      // expected right neighbor name
        "Terrain_(-10.0, 0.0, -15.0)")]    // expected bottom neighbor name


    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        -5.0f, 0.0f, 10.0f,    // position x,y,z,
        "Terrain_(-10.0, 0.0, 10.0)",     // expected left neighbor name
        "Terrain_(-5.0, 0.0, 15.0)",      // expected top neighbor name
        "Terrain_(0.0, 0.0, 10.0)",      // expected right neighbor name
        "Terrain_(-5.0, 0.0, 5.0)")]    // expected bottom neighbor name
    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        -5.0f, 0.0f, 5.0f,    // position x,y,z,
        "Terrain_(-10.0, 0.0, 5.0)",     // expected left neighbor name
        "Terrain_(-5.0, 0.0, 10.0)",      // expected top neighbor name
        "Terrain_(0.0, 0.0, 5.0)",      // expected right neighbor name
        "Terrain_(-5.0, 0.0, 0.0)")]    // expected bottom neighbor name
    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        -5.0f, 0.0f, 0.0f,    // position x,y,z,
        "Terrain_(-10.0, 0.0, 0.0)",     // expected left neighbor name
        "Terrain_(-5.0, 0.0, 5.0)",      // expected top neighbor name
        "Terrain_(0.0, 0.0, 0.0)",      // expected right neighbor name
        "Terrain_(-5.0, 0.0, -5.0)")]    // expected bottom neighbor name
   [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        -5.0f, 0.0f, -5.0f,    // position x,y,z,
        "Terrain_(-10.0, 0.0, -5.0)",     // expected left neighbor name
        "Terrain_(-5.0, 0.0, 0.0)",      // expected top neighbor name
        "Terrain_(0.0, 0.0, -5.0)",      // expected right neighbor name
        "Terrain_(-5.0, 0.0, -10.0)")]    // expected bottom neighbor name
    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        -5.0f, 0.0f, -10.0f,    // position x,y,z,
        "Terrain_(-10.0, 0.0, -10.0)",     // expected left neighbor name
        "Terrain_(-5.0, 0.0, -5.0)",      // expected top neighbor name
        "Terrain_(0.0, 0.0, -10.0)",      // expected right neighbor name
        "Terrain_(-5.0, 0.0, -15.0)")]    // expected bottom neighbor name


    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        0.0f, 0.0f, 10.0f,    // position x,y,z,
        "Terrain_(-5.0, 0.0, 10.0)",     // expected left neighbor name
        "Terrain_(0.0, 0.0, 15.0)",      // expected top neighbor name
        "Terrain_(5.0, 0.0, 10.0)",      // expected right neighbor name
        "Terrain_(0.0, 0.0, 5.0)")]    // expected bottom neighbor name
    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        0.0f, 0.0f, 5.0f,    // position x,y,z,
        "Terrain_(-5.0, 0.0, 5.0)",     // expected left neighbor name
        "Terrain_(0.0, 0.0, 10.0)",      // expected top neighbor name
        "Terrain_(5.0, 0.0, 5.0)",      // expected right neighbor name
        "Terrain_(0.0, 0.0, 0.0)")]    // expected bottom neighbor name
    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        0.0f, 0.0f, 0.0f,    // position x,y,z,
        "Terrain_(-5.0, 0.0, 0.0)",     // expected left neighbor name
        "Terrain_(0.0, 0.0, 5.0)",      // expected top neighbor name
        "Terrain_(5.0, 0.0, 0.0)",      // expected right neighbor name
        "Terrain_(0.0, 0.0, -5.0)")]    // expected bottom neighbor name
   [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        0.0f, 0.0f, -5.0f,    // position x,y,z,
        "Terrain_(-5.0, 0.0, -5.0)",     // expected left neighbor name
        "Terrain_(0.0, 0.0, 0.0)",      // expected top neighbor name
        "Terrain_(5.0, 0.0, -5.0)",      // expected right neighbor name
        "Terrain_(0.0, 0.0, -10.0)")]    // expected bottom neighbor name
    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        0.0f, 0.0f, -10.0f,    // position x,y,z,
        "Terrain_(-5.0, 0.0, -10.0)",     // expected left neighbor name
        "Terrain_(0.0, 0.0, -5.0)",      // expected top neighbor name
        "Terrain_(5.0, 0.0, -10.0)",      // expected right neighbor name
        "Terrain_(0.0, 0.0, -15.0)")]    // expected bottom neighbor name


    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        5.0f, 0.0f, 10.0f,    // position x,y,z,
        "Terrain_(0.0, 0.0, 10.0)",     // expected left neighbor name
        "Terrain_(5.0, 0.0, 15.0)",      // expected top neighbor name
        "Terrain_(10.0, 0.0, 10.0)",      // expected right neighbor name
        "Terrain_(5.0, 0.0, 5.0)")]    // expected bottom neighbor name
    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        5.0f, 0.0f, 5.0f,    // position x,y,z,
        "Terrain_(0.0, 0.0, 5.0)",     // expected left neighbor name
        "Terrain_(5.0, 0.0, 10.0)",      // expected top neighbor name
        "Terrain_(10.0, 0.0, 5.0)",      // expected right neighbor name
        "Terrain_(5.0, 0.0, 0.0)")]    // expected bottom neighbor name
    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        5.0f, 0.0f, 0.0f,    // position x,y,z,
        "Terrain_(0.0, 0.0, 0.0)",     // expected left neighbor name
        "Terrain_(5.0, 0.0, 5.0)",      // expected top neighbor name
        "Terrain_(10.0, 0.0, 0.0)",      // expected right neighbor name
        "Terrain_(5.0, 0.0, -5.0)")]    // expected bottom neighbor name
   [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        5.0f, 0.0f, -5.0f,    // position x,y,z,
        "Terrain_(0.0, 0.0, -5.0)",     // expected left neighbor name
        "Terrain_(5.0, 0.0, 0.0)",      // expected top neighbor name
        "Terrain_(10.0, 0.0, -5.0)",      // expected right neighbor name
        "Terrain_(5.0, 0.0, -10.0)")]    // expected bottom neighbor name
    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        5.0f, 0.0f, -10.0f,    // position x,y,z,
        "Terrain_(0.0, 0.0, -10.0)",     // expected left neighbor name
        "Terrain_(5.0, 0.0, -5.0)",      // expected top neighbor name
        "Terrain_(10.0, 0.0, -10.0)",      // expected right neighbor name
        "Terrain_(5.0, 0.0, -15.0)")]    // expected bottom neighbor name
   

    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        10.0f, 0.0f, 10.0f,    // position x,y,z,
        "Terrain_(5.0, 0.0, 10.0)",     // expected left neighbor name
        "Terrain_(10.0, 0.0, 15.0)",      // expected top neighbor name
        "Terrain_(15.0, 0.0, 10.0)",      // expected right neighbor name
        "Terrain_(10.0, 0.0, 5.0)")]    // expected bottom neighbor name
    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        10.0f, 0.0f, 5.0f,    // position x,y,z,
        "Terrain_(5.0, 0.0, 5.0)",     // expected left neighbor name
        "Terrain_(10.0, 0.0, 10.0)",      // expected top neighbor name
        "Terrain_(15.0, 0.0, 5.0)",      // expected right neighbor name
        "Terrain_(10.0, 0.0, 0.0)")]    // expected bottom neighbor name
    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        10.0f, 0.0f, 0.0f,    // position x,y,z,
        "Terrain_(5.0, 0.0, 0.0)",     // expected left neighbor name
        "Terrain_(10.0, 0.0, 5.0)",      // expected top neighbor name
        "Terrain_(15.0, 0.0, 0.0)",      // expected right neighbor name
        "Terrain_(10.0, 0.0, -5.0)")]    // expected bottom neighbor name
   [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        10.0f, 0.0f, -5.0f,    // position x,y,z,
        "Terrain_(5.0, 0.0, -5.0)",     // expected left neighbor name
        "Terrain_(10.0, 0.0, 0.0)",      // expected top neighbor name
        "Terrain_(15.0, 0.0, -5.0)",      // expected right neighbor name
        "Terrain_(10.0, 0.0, -10.0)")]    // expected bottom neighbor name
    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        10.0f, 0.0f, -10.0f,    // position x,y,z,
        "Terrain_(5.0, 0.0, -10.0)",     // expected left neighbor name
        "Terrain_(10.0, 0.0, -5.0)",      // expected top neighbor name
        "Terrain_(15.0, 0.0, -10.0)",      // expected right neighbor name
        "Terrain_(10.0, 0.0, -15.0)")]    // expected bottom neighbor name


    [TestCase(
        15.0f, 10.0f, 5.0f,   // size x,y,z
        0.0f, 10.0f, 0.0f,    // position x,y,z,
        "Terrain_(-15.0, 10.0, 0.0)",     // expected left neighbor name
        "Terrain_(0.0, 10.0, 5.0)",      // expected top neighbor name
        "Terrain_(15.0, 10.0, 0.0)",     // expected right neighbor name
        "Terrain_(0.0, 10.0, -5.0)")]    // expected bottom neighbor name
    [TestCase(
        5.0f, 0.0f, 2.0f,   // size x,y,z
        0.0f, 0.0f, .0f,    // position x,y,z,
        "Terrain_(-5.0, 0.0, 0.0)",     // expected left neighbor name
        "Terrain_(0.0, 0.0, 2.0)",      // expected top neighbor name
        "Terrain_(5.0, 0.0, 0.0)",      // expected right neighbor name
        "Terrain_(0.0, 0.0, -2.0)")]    // expected bottom neighbor name
    [TestCase(
        50.0f, 0.0f, 55.0f,   // size x,y,z
        0.0f, 0.0f, .0f,    // position x,y,z,
        "Terrain_(-50.0, 0.0, 0.0)",     // expected left neighbor name
        "Terrain_(0.0, 0.0, 55.0)",      // expected top neighbor name
        "Terrain_(50.0, 0.0, 0.0)",      // expected right neighbor name
        "Terrain_(0.0, 0.0, -55.0)")]    // expected bottom neighbor name
    public void GetTerrainNeighborPrefabNames_test(float terrainSizeX, float terrainSizeY, float terrainSizeZ, 
        float terrainPositionX, float terrainPositionY, float terrainPositionZ, 
        string expectedLeftNeighborResult, string expectedTopNeighborResult,
        string expectedRightNeighborResult, string expectedBottomNeighborResult)
    {
        var navigationManager = new GameObject().AddComponent<NavigationManager>();
        
        var terrainData = new TerrainData();
        terrainData.size = new Vector3(terrainSizeX, terrainSizeY, terrainSizeZ);
        var terrain = Terrain.CreateTerrainGameObject(terrainData).GetComponent<Terrain>();
        terrain.transform.position = new Vector3(terrainPositionX, terrainPositionY, terrainPositionZ);
        var terrainNeighborNames = navigationManager.GetTerrainNeighborPrefabNames(terrain);

        Assert.AreEqual(expectedLeftNeighborResult, terrainNeighborNames.leftNeighbor,  "leftNeighbor");
        Assert.AreEqual(expectedTopNeighborResult, terrainNeighborNames.topNeighbor, "topNeighbor");
        Assert.AreEqual(expectedRightNeighborResult, terrainNeighborNames.rightNeighbor, "rightNeighbor");
        Assert.AreEqual(expectedBottomNeighborResult, terrainNeighborNames.bottomNeighbor, "bottomNeighbor");
    }


  [Test]
  [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        -10.0f, 0.0f, 10.0f,    // position x,y,z,
        -8.75f,     // expected left Threshold float
        13.75f,      // expected top Threshold float
        -6.25f,      // expected right Threshold float
        11.25f)]     // expected bottom Threshold float
    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        -10.0f, 0.0f, 5.0f,    // position x,y,z,
        -8.75f,     // expected left Threshold float
        8.75f,      // expected top Threshold float
        -6.25f,      // expected right Threshold float
        6.25f)]     // expected bottom Threshold float
    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        -10.0f, 0.0f, 0.0f,    // position x,y,z,
        -8.75f,     // expected left Threshold float
        3.75f,      // expected top Threshold float
        -6.25f,      // expected right Threshold float
        1.25f)]     // expected bottom Threshold float
   [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        -10.0f, 0.0f, -5.0f,    // position x,y,z,
        -8.75f,     // expected left Threshold float
        -1.25f,      // expected top Threshold float
        -6.25f,      // expected right Threshold float
        -3.75f)]     // expected bottom Threshold float   
    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        -10.0f, 0.0f, -10.0f,    // position x,y,z,
        -8.75f,     // expected left Threshold float
        -6.25f,      // expected top Threshold float
        -6.25f,      // expected right Threshold float
        -8.75f)]     // expected bottom Threshold float


  [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        -5.0f, 0.0f, 10.0f,    // position x,y,z,
        -3.75f,     // expected left Threshold float
        13.75f,      // expected top Threshold float
        -1.25f,      // expected right Threshold float
        11.25f)]     // expected bottom Threshold float
    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        -5.0f, 0.0f, 5.0f,    // position x,y,z,
        -3.75f,     // expected left Threshold float
        8.75f,      // expected top Threshold float
        -1.25f,      // expected right Threshold float
        6.25f)]     // expected bottom Threshold float
    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        -5.0f, 0.0f, 0.0f,    // position x,y,z,
        -3.75f,     // expected left Threshold float
        3.75f,      // expected top Threshold float
        -1.25f,      // expected right Threshold float
        1.25f)]     // expected bottom Threshold float
   [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        -5.0f, 0.0f, -5.0f,    // position x,y,z,
        -3.75f,     // expected left Threshold float
        -1.25f,      // expected top Threshold float
        -1.25f,      // expected right Threshold float
        -3.75f)]     // expected bottom Threshold float   
    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        -5.0f, 0.0f, -10.0f,    // position x,y,z,
        -3.75f,     // expected left Threshold float
        -6.25f,      // expected top Threshold float
        -1.25f,      // expected right Threshold float
        -8.75f)]     // expected bottom Threshold float


    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        0.0f, 0.0f, 10.0f,    // position x,y,z,
        1.25f,     // expected left Threshold float
        13.75f,      // expected top Threshold float
        3.75f,      // expected right Threshold float
        11.25f)]     // expected bottom Threshold float
    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        0.0f, 0.0f, 5.0f,    // position x,y,z,
        1.25f,     // expected left Threshold float
        8.75f,      // expected top Threshold float
        3.75f,      // expected right Threshold float
        6.25f)]     // expected bottom Threshold float
    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        0.0f, 0.0f, 0.0f,    // position x,y,z,
        1.25f,     // expected left Threshold float
        3.75f,      // expected top Threshold float
        3.75f,      // expected right Threshold float
        1.25f)]     // expected bottom Threshold float
   [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        0.0f, 0.0f, -5.0f,    // position x,y,z,
        1.25f,     // expected left Threshold float
        -1.25f,      // expected top Threshold float
        3.75f,      // expected right Threshold float
        -3.75f)]     // expected bottom Threshold float   
    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        0.0f, 0.0f, -10.0f,    // position x,y,z,
        1.25f,     // expected left Threshold float
        -6.25f,      // expected top Threshold float
        3.75f,      // expected right Threshold float
        -8.75f)]     // expected bottom Threshold float


    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        5.0f, 0.0f, 10.0f,    // position x,y,z,
        6.25f,     // expected left Threshold float
        13.75f,      // expected top Threshold float
        8.75f,      // expected right Threshold float
        11.25f)]     // expected bottom Threshold float
    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        5.0f, 0.0f, 5.0f,    // position x,y,z,
        6.25f,     // expected left Threshold float
        8.75f,      // expected top Threshold float
        8.75f,      // expected right Threshold float
        6.25f)]     // expected bottom Threshold float
    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        5.0f, 0.0f, 0.0f,    // position x,y,z,
        6.25f,     // expected left Threshold float
        3.75f,      // expected top Threshold float
        8.75f,      // expected right Threshold float
        1.25f)]     // expected bottom Threshold float
   [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        5.0f, 0.0f, -5.0f,    // position x,y,z,
        6.25f,     // expected left Threshold float
        -1.25f,      // expected top Threshold float
        8.75f,      // expected right Threshold float
        -3.75f)]     // expected bottom Threshold float   
    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        5.0f, 0.0f, -10.0f,    // position x,y,z,
        6.25f,     // expected left Threshold float
        -6.25f,      // expected top Threshold float
        8.75f,      // expected right Threshold float
        -8.75f)]     // expected bottom Threshold float 


    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        10.0f, 0.0f, 10.0f,    // position x,y,z,
        11.25f,     // expected left Threshold float
        13.75f,      // expected top Threshold float
        13.75f,      // expected right Threshold float
        11.25f)]     // expected bottom Threshold float
    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        10.0f, 0.0f, 5.0f,    // position x,y,z,
        11.25f,     // expected left Threshold float
        8.75f,      // expected top Threshold float
        13.75f,      // expected right Threshold float
        6.25f)]     // expected bottom Threshold float
    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        10.0f, 0.0f, 0.0f,    // position x,y,z,
        11.25f,     // expected left Threshold float
        3.75f,      // expected top Threshold float
        13.75f,      // expected right Threshold float
        1.25f)]     // expected bottom Threshold float
   [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        10.0f, 0.0f, -5.0f,    // position x,y,z,
        11.25f,     // expected left Threshold float
        -1.25f,      // expected top Threshold float
        13.75f,      // expected right Threshold float
        -3.75f)]     // expected bottom Threshold float   
    [TestCase(
        5.0f, 0.0f, 5.0f,   // size x,y,z,
        10.0f, 0.0f, -10.0f,    // position x,y,z,
        11.25f,     // expected left Threshold float
        -6.25f,      // expected top Threshold float
        13.75f,      // expected right Threshold float
        -8.75f)]     // expected bottom Threshold float 


    [TestCase(
        15.0f, 10.0f, 5.0f,   // size x,y,z
        0.0f, 10.0f, 0.0f,    // position x,y,z,
        3.75f,     // expected left Threshold float
        3.75f,      // expected top Threshold float
        11.25f,      // expected right Threshold float
        1.25f)]     // expected bottom Threshold float 
    [TestCase(
        5.0f, 0.0f, 2.0f,   // size x,y,z
        0.0f, 0.0f, .0f,    // position x,y,z,
        1.25f,     // expected left Threshold float
        1.5f,      // expected top Threshold float
        3.75f,      // expected right Threshold float
        0.5f)]     // expected bottom Threshold float 
    [TestCase(
        50.0f, 0.0f, 55.0f,   // size x,y,z
        0.0f, 0.0f, .0f,    // position x,y,z,
        12.5f,     // expected left Threshold float
        41.25f,      // expected top Threshold float
        37.5f,      // expected right Threshold float
        13.75f)]     // expected bottom Threshold float 
        
    public void GetTerrainThresholds_test(float terrainSizeX, float terrainSizeY, float terrainSizeZ, 
        float terrainPositionX, float terrainPositionY, float terrainPositionZ, 
        float expectedLeftThresholdResult, float expectedTopThresholdResult,
        float expectedRightThresholdResult, float expectedBottomThresholdResult)
    {
        var navigationManager = new GameObject().AddComponent<NavigationManager>();
        
        var terrainData = new TerrainData();
        terrainData.size = new Vector3(terrainSizeX, terrainSizeY, terrainSizeZ);
        var terrain = Terrain.CreateTerrainGameObject(terrainData).GetComponent<Terrain>();
        terrain.transform.position = new Vector3(terrainPositionX, terrainPositionY, terrainPositionZ);
        var terrainThrehsolds = navigationManager.GetTerrainNeighborThresholds(terrain);

        Assert.AreEqual(expectedLeftThresholdResult, terrainThrehsolds.leftThreshold,  "leftThreshold");
        Assert.AreEqual(expectedTopThresholdResult, terrainThrehsolds.topThreshold, "topThreshold");
        Assert.AreEqual(expectedRightThresholdResult, terrainThrehsolds.rightThreshold, "rightThreshold");
        Assert.AreEqual(expectedBottomThresholdResult, terrainThrehsolds.bottomThreshold, "bottomThreshold");
    }

}
