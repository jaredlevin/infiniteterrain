using UnityEngine;
using UnityEngine.AI;
using System;


public class NavigationManager : MonoBehaviour
{
    private Error errorManager; // standard error manager
    public Player player;  // object we move (the main character)

    // when the player reaches 1/thresholdFraction we will begin loading the next terrain square 
    // ex. thersholdFraction of 4 loads the next terrain when the player reaches 25% or 1/4 close to an edge
    // ex. thersholdFraction of 8 loads the next terrain when the player reaches 12.5% or 1/8 close to an edge
    public int thresholdFraction;  

    private NavMeshAgent navMeshAgent;  

    private Animator playerAnimation;

    private float speed;

    private TERRAIN_NEIGHBOR_THRESHOLDS currentNeighborTerrainThresholds;
    private TERRAIN_NEIGHBOR_PREFAB_NAMES currentNeighborTerrainNames;

    // always need one Terrain in the scene to start
    // this is our base terrain from which all other terrains are drawn as the player moves
    public Terrain baseTerrain;  

    // current terrain the nav mesg agent is walking on
    private Terrain currentTerrain;

    public struct TERRAIN_NEIGHBOR_THRESHOLDS { 
        public float leftThreshold;
        public float topThreshold;
        public float rightThreshold;
        public float bottomThreshold;
    }

    public struct TERRAIN_NEIGHBOR_PREFAB_NAMES {
        public string leftNeighbor;
        public string topNeighbor;
        public string rightNeighbor;
        public string bottomNeighbor;
    }

// load the prefab names of our neighboring terrains
public TERRAIN_NEIGHBOR_PREFAB_NAMES GetTerrainNeighborPrefabNames(Terrain terrain) {
    
    TERRAIN_NEIGHBOR_PREFAB_NAMES terrainNeighborPrefabNames = new TERRAIN_NEIGHBOR_PREFAB_NAMES();
        terrainNeighborPrefabNames.leftNeighbor = "Terrain_(" + (terrain.GetPosition().x - terrain.terrainData.size.x) + ".0, " + (terrain.GetPosition().y) + ".0, " + (terrain.GetPosition().z) + ".0)";
        terrainNeighborPrefabNames.topNeighbor = "Terrain_(" + (terrain.GetPosition().x) + ".0, " + (terrain.GetPosition().y) + ".0, " + (terrain.GetPosition().z + terrain.terrainData.size.z) + ".0)";
        terrainNeighborPrefabNames.rightNeighbor = "Terrain_(" + (terrain.GetPosition().x + terrain.terrainData.size.x) + ".0, " + (terrain.GetPosition().y) + ".0, " + (terrain.GetPosition().z) + ".0)";
        terrainNeighborPrefabNames.bottomNeighbor = "Terrain_(" + (terrain.GetPosition().x) + ".0, " + (terrain.GetPosition().y) + ".0, " + (terrain.GetPosition().z - terrain.terrainData.size.z) + ".0)";

    return terrainNeighborPrefabNames;

}

// get the borders of our current terrain, this is needed to determine which terrain the navmesh agent is currently walking on
public Rect GetTerrainBordersRect(Terrain terrain) {

    Rect rectBorders = new Rect();
    rectBorders.x = terrain.transform.position.x;
    rectBorders.y = terrain.transform.position.z;
    rectBorders.width = terrain.terrainData.size.x;
    rectBorders.height = terrain.terrainData.size.z;
    Debug.Log(rectBorders.x + " " + rectBorders.y + " " + rectBorders.width + " " + rectBorders.height);  

    return rectBorders;
}

// calculate the position of the navmeshagent on each edge where we should load the next terrain
public TERRAIN_NEIGHBOR_THRESHOLDS GetTerrainNeighborThresholds(Terrain terrain) {
    
    TERRAIN_NEIGHBOR_THRESHOLDS terrainNeighborThresholds = new TERRAIN_NEIGHBOR_THRESHOLDS();
        terrainNeighborThresholds.leftThreshold =  ((terrain.terrainData.size.x) / thresholdFraction) + terrain.GetPosition().x;
        terrainNeighborThresholds.topThreshold =   (terrain.terrainData.size.z - ((terrain.terrainData.size.z) / thresholdFraction)) + terrain.GetPosition().z;
        terrainNeighborThresholds.rightThreshold = (terrain.terrainData.size.x - ((terrain.terrainData.size.x) / thresholdFraction)) + terrain.GetPosition().x;
        terrainNeighborThresholds.bottomThreshold =  ((terrain.terrainData.size.z) / thresholdFraction) + terrain.GetPosition().z;

    return terrainNeighborThresholds;

}

    // Start is called before the first frame update
    void Start()
    {
        errorManager = FindObjectOfType<Error>();
        playerAnimation = GetComponentInChildren<Animator>();
        
        navMeshAgent = GetComponentInParent<NavMeshAgent>();
        navMeshAgent.updatePosition = false;

       // baseTerrain = GameObject.FindObjectOfType<Terrain>();
        currentTerrain = baseTerrain;
        currentNeighborTerrainThresholds = GetTerrainNeighborThresholds(currentTerrain);
        currentNeighborTerrainNames = GetTerrainNeighborPrefabNames(currentTerrain);
       // DrawNeighborTerrainThresholds();
    }

    void Update()
    {
        // walk
        if (((Input.touchCount == 1) || Input.GetMouseButton(0)) && ((Input.touchCount != 2))) {
            Vector3 touchOrMousePoint;
            if (Input.touchCount == 1) {
                touchOrMousePoint = Input.touches[0].position;
            }
            else {
                touchOrMousePoint = Input.mousePosition;     
            }

            RaycastHit hit;
            
            if (Physics.Raycast(Camera.main.ScreenPointToRay(touchOrMousePoint), out hit, 100)) {
                StartWalk(hit.point);
            }
            
            // check borders
            Rect terrainBorders = GetTerrainBordersRect(currentTerrain);
            if (!terrainBorders.Contains(new Vector2(player.transform.position.x, player.transform.position.z)))
            {
                Debug.Log("Player is outside terrain borders");
                Terrain nextTerrain = null;
                // find which terrain we are on
                if (currentTerrain.leftNeighbor != null) {
                    terrainBorders = GetTerrainBordersRect(currentTerrain.leftNeighbor);
                    if (terrainBorders.Contains(new Vector2(player.transform.position.x, player.transform.position.z)))
                    {
                        nextTerrain = currentTerrain.leftNeighbor;
                    }
                }
                if (currentTerrain.topNeighbor != null) {
                    terrainBorders = GetTerrainBordersRect(currentTerrain.topNeighbor);
                    if (terrainBorders.Contains(new Vector2(player.transform.position.x, player.transform.position.z)))
                    {
                        nextTerrain = currentTerrain.topNeighbor;
                    }
                }
                if (currentTerrain.rightNeighbor != null) {
                    terrainBorders = GetTerrainBordersRect(currentTerrain.rightNeighbor);
                    if (terrainBorders.Contains(new Vector2(player.transform.position.x, player.transform.position.z)))
                    {
                        nextTerrain = currentTerrain.rightNeighbor;
                    }
                }
                if (currentTerrain.bottomNeighbor != null) {
                    terrainBorders = GetTerrainBordersRect(currentTerrain.bottomNeighbor);
                    if (terrainBorders.Contains(new Vector2(player.transform.position.x, player.transform.position.z)))
                    {
                        nextTerrain = currentTerrain.bottomNeighbor;
                    }
                }
                
                if (nextTerrain != null) {
                currentTerrain = nextTerrain;
                Debug.Log("currentTerrain=" + currentTerrain.name);
                currentNeighborTerrainThresholds = GetTerrainNeighborThresholds(currentTerrain);
                currentNeighborTerrainNames = GetTerrainNeighborPrefabNames(currentTerrain);
                }
            }

            NavMeshHit nmHit;
            if (navMeshAgent.FindClosestEdge(out nmHit)) {
                
                if (player.transform.position.x <= currentNeighborTerrainThresholds.leftThreshold) {
                    Debug.Log("Load left");
                    if (currentTerrain.leftNeighbor == null)
                        currentTerrain.SetNeighbors(((GameObject) Instantiate(Resources.Load("Terrain/" + currentNeighborTerrainNames.leftNeighbor))).GetComponent<Terrain>(), currentTerrain.topNeighbor, currentTerrain.rightNeighbor, currentTerrain.bottomNeighbor);
                }
                else if (player.transform.position.x >= currentNeighborTerrainThresholds.rightThreshold) {
                    Debug.Log("Load right");
                    if (currentTerrain.rightNeighbor == null)
                        currentTerrain.SetNeighbors(currentTerrain.leftNeighbor, currentTerrain.topNeighbor, ((GameObject) Instantiate(Resources.Load("Terrain/" + currentNeighborTerrainNames.rightNeighbor))).GetComponent<Terrain>(), currentTerrain.bottomNeighbor);
                }
                else if (player.transform.position.z >= currentNeighborTerrainThresholds.topThreshold) {
                    Debug.Log("Load top");
                    if (currentTerrain.topNeighbor == null)
                        currentTerrain.SetNeighbors(currentTerrain.leftNeighbor, ((GameObject) Instantiate(Resources.Load("Terrain/" + currentNeighborTerrainNames.topNeighbor))).GetComponent<Terrain>(), currentTerrain.rightNeighbor, currentTerrain.bottomNeighbor);
                }
                else if (player.transform.position.z <= currentNeighborTerrainThresholds.bottomThreshold) {
                    Debug.Log("Load bottom");
                    if (currentTerrain.bottomNeighbor == null)
                        currentTerrain.SetNeighbors(currentTerrain.leftNeighbor, currentTerrain.topNeighbor, currentTerrain.rightNeighbor, ((GameObject) Instantiate(Resources.Load("Terrain/" + currentNeighborTerrainNames.bottomNeighbor))).GetComponent<Terrain>());
                }
                else {
                    // unload all neighbors
                    if (currentTerrain.leftNeighbor != null)
                        Destroy(currentTerrain.leftNeighbor.gameObject);
                    if (currentTerrain.topNeighbor != null)
                        Destroy(currentTerrain.topNeighbor.gameObject);
                    if (currentTerrain.rightNeighbor != null)
                        Destroy(currentTerrain.rightNeighbor.gameObject);
                    if (currentTerrain.bottomNeighbor != null)
                        Destroy(currentTerrain.bottomNeighbor.gameObject);
                }
            
            }
        }
        else {
            StopWalk();
        }
    
        
        
    
    }

    void StartWalk(Vector3 destination)
    {
      //  Debug.Log(destination);
        navMeshAgent.SetDestination(destination);
        speed = 1.0f;
        playerAnimation.SetFloat("locomotion", speed);
        navMeshAgent.speed = speed;
        navMeshAgent.isStopped = false;
    }
  
    void StopWalk()
    {
        speed = 0.0f;
        playerAnimation.SetFloat("locomotion", speed);
        navMeshAgent.velocity = Vector3.zero;
        navMeshAgent.isStopped = true;
    }
  
    void OnAnimatorMove() {
        // Update position to agent position
        player.transform.position = navMeshAgent.nextPosition;
    }

}
